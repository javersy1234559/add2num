# ADD 2 SUM

Dự án giúp tính tổng của hai số lớn.

## Hướng Dẫn Chạy Test Cases

Để chạy test cases trong dự án, bạn cần cài đặt [Java Development Kit (JDK)](https://www.oracle.com/java/technologies/javase-downloads.html) và [Apache Maven](https://maven.apache.org/download.cgi).

### Bước 1: Sao chép dự án về máy

```bash
git clone https://gitlab.com/javersy1234559/add2num.git
cd add2num
```
### Bước 2: Chạy Test Cases
```mvn test```

# Hướng dẫn Chạy Dự án Maven với Java 17 trên IntelliJ IDEA

## Bước 1: Cài đặt IntelliJ IDEA

Đầu tiên, hãy cài đặt IntelliJ IDEA trên máy tính của bạn. Bạn có thể tải IntelliJ IDEA từ trang web chính thức: [https://www.jetbrains.com/idea/](https://www.jetbrains.com/idea/).

## Bước 2: Cài đặt JDK 17

Chắc chắn rằng bạn đã cài đặt JDK 17 trên máy tính của mình. Bạn có thể tải JDK 17 từ trang web của Oracle hoặc OpenJDK.

## Bước 3: Import Dự án vào IntelliJ IDEA

1. Mở IntelliJ IDEA và chọn `File > Open`.
2. Chọn thư mục chứa dự án Maven của bạn và nhấn `Open`.

## Bước 4: Cập nhật pom.xml
Mở file `pom.xml` và bấm vào biểu tưởng maven góc phải

## Bước 5: Chạy ứng dụng
1. Mở src/main/java và tìm class có phương thức main.
2. Chuột phải vào class và chọn Run 'Tên_Class' (ví dụ: Run 'MainApplication').
3. Chọn SDK là JDK 17.