package org.example;

import java.util.logging.Logger;

public class MyBigNumber {

    private String addZeroToString(String str, int size) {
        for (int i = 0; i < size; i++) {
            str = '0' + str;
        }
        return str;
    }

    /**
     * Calculates the sum of two positive numbers represented as strings.
     * The input numbers should contain only non-negative integers.
     *
     * @param num1 The first positive number as a string.
     * @param num2 The second positive number as a string.
     * @return The sum of the two input numbers as a string.
     */
    public String sum(String num1, String num2) {

        if (num1.length() > num2.length()) num2 = addZeroToString(num2, num1.length() - num2.length());
        else if (num2.length() > num1.length()) num1 = addZeroToString(num1, num2.length() - num1.length());

        String result = "";
        int du = 0;
        for (int i = num1.length() - 1; i >= 0; i--) {
            int temp = du + Integer.parseInt(num1.substring(i, i + 1)) + Integer.parseInt(num2.substring(i, i + 1));
            du = temp / 10;
            int last = temp % 10;
            result = String.valueOf(last) + result;
        }
        if (du > 0) {
            result = '1' + result;
        }

        return result;
    }

}
