package org.example;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        String num1 = "123456789";
        String num2 = "987654321";
        MyBigNumber myBigNumber = new MyBigNumber();
        String result = myBigNumber.sum(num1, num2);
        System.out.println(result);
    }
}