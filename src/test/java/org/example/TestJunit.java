package org.example;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestJunit {

    @Test
    public void add2NumTest()
    {
        MyBigNumber myBigNumber = new MyBigNumber();

        // Test case 1: Check simple integer number
        String result1 = myBigNumber.sum("123", "456");
        assertEquals("579", result1);

        // Test case 2: Check big num and small number
        String result2 = myBigNumber.sum("999999999999999999999999999", "1");
        assertEquals("1000000000000000000000000000", result2);

        // Test case  3 : Check both number is zero
        String result3 = myBigNumber.sum("0", "0");
        assertEquals("0", result3);

        // Test case  4 : Check both number is big number
        String result4 = myBigNumber.sum("123456789", "987654321");
        assertEquals("1111111110", result4);

//        Test case 4 : With a zero a head (This will fail because not a right format of integer )
//        String result4 = myBigNumber.sum("012", "0123");
//        assertEquals(" ", result4);

//        // Test case  4 : Check both number is negative
//        String result4 = myBigNumber.sum("-34", "44");
//        assertEquals("-78", result4);
//
//        // Test case 5: Check negative and positive big number
//        String result5 = myBigNumber.sum("-123456789", "-987654321");
//        assertEquals("-864197532", result5);

    }
}
